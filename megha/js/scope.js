

var restaurants = [
    {
        name:"A&W Canada",
        loction:"5009 Ellerslie Rd SW, Edmonton, AB T6X 1X2",
        latitude:"53.424301",
        longitude:"113.420770",
    },
    {
        name:"The Chopped Leaf",
        loction:"HARVEST POINTE, 5063 Ellerslie Rd SW, Edmonton, AB T6X 1A4",
        latitude:"53.423437",
        longitude:"113.418828",
    },
    {
        name:"Subway",
        loction:"10700-104 Ave Grant MacEwan College,Edmonton, AB T5J 4S2",
        latitude:"53.547133",
        longitude:"113.502787",
    },
    {
        name:"McDonald's",
        loction:"274, Edmonton, AB T5J 4G8",
        latitude:"53.545863",
        longitude:"113.503435",
    },
    {
        name:"Denny's",
        loction:"10803 104 Ave NW, Edmonton, AB T5J 4Z5",
        latitude:"53.546519",
        longitude:"113.506359",
    },

    {
        name:"Boston Pizza",
        loction:"585 St Albert Trail #80, St. Albert, AB T8N 6G5",
        latitude:"53.659013",
        longitude:"113.582709",
}];

for (var i = 0; i<restaurants.length; i++){

var button = document.createElement('button');
var text = document.createTextNode(restaurants[i].name);
// console.log(restaurants[i].name);
button.appendChild(text);
button.classList.add('btn');
button.classList.add('btn-primary');
button.classList.add('btn-restaurant');

button.addEventListener("click",function(e){
    alert ("I want to eat at " + this.innerText);

}
);

var target = document.getElementById('buttons');
target.appendChild(button);

}
