<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          <link rel="stylesheet" href="weather.css" >

        <title>Weather</title>
    </head>
    <body  style="background-image:url('https://images.unsplash.com/photo-1514527893004-f77d3d773644?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1401&q=80');">
        <div class="main" style="text-align: center;">
         <h1>
              Weather
         </h1>
        <br />

        <input type="text" id="name" />
        <input type="text" id="humidity" />
        <input type="text" id="temperature" />
        <input type="text" id="weather-main" />
        <button id="getWeatherButton">Get Weather!</button>

        </div>
        <div class="main1" style="text-align: center;">
         <h1>
             Todays Weather
         </h1>
        <br />

        <input type="text" id="cityName-input" />
        <input type="text" id="humidity-display" />
        <input type="text" id="temperature-display" />
        <input type="text" id="weather-main-display" />
        <button id="getWeatherButton1">Get Weather!</button>


        </div>
        <div class="row">
            <div class="map" >
                <iframe id="mapdisplay" style="align= center;">
                        <br/>map
                </iframe>
            <br/>map
            </div>
        </div>


    </body>
    <script src="weather.js"></script>
</html>
