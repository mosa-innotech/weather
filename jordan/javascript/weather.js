document.getElementById('weather').addEventListener('click', function(){
    fetch('http://api.openweathermap.org/data/2.5/weather?q=montreal&APPID=d65d85d92a3b330454ffa21c6e04181a')
        .then(
            function(response){
                response.json().then(function(data){
                    console.log(data);
                    console.log(data.name);
                    console.log(data.main.temp);
                    console.log(data.main.humidity);
                    console.log(data.weather["0"].description);

                    document.getElementById('weather-main').style.display = "block";

                    document.getElementById('city-display').innerHTML = data.name;
                    document.getElementById('tempature-display').innerHTML = data.main.temp;
                    document.getElementById('humidity-display').innerHTML = data.main.humidity;
                    document.getElementById('description-display').innerHTML = data.weather["0"].description;

                    var longitude = data.coord.lon;
                    var latitude = data.coord.lat;
                    var mapUrl = "http://maps.google.com/maps?q="+ latitude +","+ longitude+"&z=15&output=embed";
                    document.getElementById('map-iframe').src = mapUrl;
                });
            }
        );


});
document.getElementById('weather2').addEventListener('click', function(event){
    var cityName = document.getElementById("search").value;
    var apiUrl = "http://api.openweathermap.org/data/2.5/weather?q="+ cityName +"&APPID=d65d85d92a3b330454ffa21c6e04181a";

    fetch(apiUrl)
        .then(
            function(response){
                response.json().then(function(data){

                    document.getElementById('weather-main').style.display = "block";

                    document.getElementById('city-display').innerHTML = data.name;
                    document.getElementById('tempature-display').innerHTML = kelvinToCelsius(data.main.temp).toFixed(0) + " *C";
                    document.getElementById('humidity-display').innerHTML = data.main.humidity;
                    document.getElementById('description-display').innerHTML = data.weather["0"].description;

                    var longitude = data.coord.lon;
                    var latitude = data.coord.lat;
                    var mapUrl = "http://maps.google.com/maps?q="+ latitude +","+ longitude+"&z=15&output=embed";
                    document.getElementById('map-iframe').src = mapUrl;
                });
            }
        );


    event.preventDefault();
});

function kelvinToCelsius(tempature){
    return tempature - 273.15;
}
