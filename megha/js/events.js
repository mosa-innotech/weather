document.getElementById("myBtn").addEventListener("click", displayDate);

function displayDate() {
  document.getElementById("demo").innerHTML = Date();
}

document.getElementById("myForm").addEventListener("submit", myFunction);

function myFunction(e) {
  alert("The form was submitted");
  e.preventDefault();
}

document.getElementById("fname1").addEventListener("blur", myFunction);

function myFunction() {
  alert("Input field lost focus.");
}
document.getElementById("sample").addEventListener("mouseenter", mouseEnter);
document.getElementById("sample").addEventListener("mouseleave", mouseLeave);

function mouseEnter() {
  document.getElementById("sample").style.color = "red";
}

function mouseLeave() {
  document.getElementById("sample").style.color = "black";
}

document.getElementById("demo5").addEventListener("mousedown", mouseDown);
document.getElementById("demo5").addEventListener("mouseup", mouseUp);

function mouseDown() {
  document.getElementById("demo5").innerHTML = "The mouse button is held down.";
}

function mouseUp() {
  document.getElementById("demo5").innerHTML = "You released the mouse button.";
}

// document.getElementById("sample2").addEventListener("change", myFunction);
//
// function myFunction() {
//   var x = document.getElementById("sample2");
//   x.value = x.value.toUpperCase();
// }
document.onkeyup = function(e){
    if(e.which == 77){
        alert ("M key was pressed");
    }  else if(e.ctrlKey && e.which == 66){
        alert("Ctrl + B shortcut combination was pressed");
    }else if(e.ctrlKey && e.altkey && e.which == 89){
        alert("Ctrl + Alt + Y shortcut combination was pressed");
    } else if(e.ctrlKey && e.altkey && e.shiftKey && e.which == 85){
        alert("Ctrl + Alt + Shift + U shortcut combination was pressed");
    }
};
