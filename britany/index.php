<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/master.css">
        <title></title>
    </head>
    <body>
        <button id='button0' type="" name="button">Weather</button>
        <div id="weathercontainer0" class='weathercontainer0'>
            <table>
                <tr>
                    <td>City:</td>
                    <td><div id="city-display"></div></td>
                </tr>
                <tr>
                    <td>Temperature:</td>
                    <td><div id="Temp-display"></div></td>
                </tr>
                <tr>
                    <td>Humidity:</td>
                    <td><div id="humid-display"></div></td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td><div id="desc-display"></div></td>
                </tr>
            </table>
        </div>
        <hr>
        <div class="container">

            <input id='search' type="text">
            <button id='button1' type="" name="button">submit</button>

            <div id="weathercontainer1" class='weathercontainer1'>
                <table>
                    <tr>
                        <td>City:</td>
                        <td><div id="city-display1"></div></td>
                    </tr>
                    <tr>
                        <td>Temperature:</td>
                        <td><div id="Temp-display1"></div></td>
                    </tr>
                    <tr>
                        <td>Humidity:</td>
                        <td><div id="humid-display1"></div></td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td><div id="desc-display1"></div></td>
                    </tr>
                </table>
            </div>
            <div id="map">
                <iframe id ='mapdisplay' src=""
                width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>
        </div>


        <script src='index.js'></script>
    </body>
</html>
