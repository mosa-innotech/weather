<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/mystyle.css" rel="stylesheet">
        <title>Weather</title>
    </head>
    <body class="text-center" data-gr-c-s-loaded="true">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
            <header class="masthead mb-auto">
                <div class="inner">
                    <h3 class="masthead-brand">Cover</h3>
                    <nav class="nav nav-masthead justify-content-center">
                        <a class="nav-link active" href="weather.php">Home</a>
                        <a class="nav-link" href="weather">Weather</a>
                        <a class="nav-link" href="about">About</a>
                    </nav>
                </div>
            </header>

            <main role="main" class="inner cover" id="home">
                <h1 class="cover-heading">The Weather Today is..</h1>
                <p class="lead">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div id="weather-main">
                                <table>
                                    <tr>
                                        <td>
                                            City:
                                        </td>
                                        <td>
                                            <div id="city-display"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tempature:
                                        </td>
                                        <td>
                                            <div id="tempature-display"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Humidity:
                                        </td>
                                        <td>
                                            <div id="humidity-display"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description:
                                        </td>
                                        <td>
                                            <div id="description-display"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div id="map">
                                <iframe id="map-iframe" src=""
                                width="400" height="300" frameborder="0" style="border:0" allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    </div>
                </p>

                <p class="lead">
                    <button id="weather" class="btn btn-lg btn-secondary" type="button" name="button">weather</button>
                </p>
                <p class="lead">
                    <form>
                        <input id="search" type="search" />
                        <button id="weather2" class="btn btn-md btn-secondary" type="button" name="button2">weather</button>

                    </form>
                </p>
            </main>

            <footer class="mastfoot mt-auto">
                <div class="inner">
                    <p>Cover template for <a href="https://getbootstrap.com/">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                </div>
            </footer>
        </div>

        <script src="javascript/weather.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
